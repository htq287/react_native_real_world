# Requirements
- Node `brew install node`
- ]Watchman](https://facebook.github.io/watchman/docs/install.html) `brew install watchman`
- JDKs
  ```sh
  $ brew tap AdoptOpenJDK/openjdk
  $ brew cask install adoptopenjdk8
  ```
- the React Native command line interface ``sudo npm install -g react-native-cli``
- [Database](https://realm.io/docs/javascript/latest/)
  - add the realm dependency `$ npm install --save realm`
  - link your project to the realm native module `react-native link realm`

# Creating a new application
```sh
$ react-native init react_native_real_world
```
# Usages
- Clone repository
```sh
$ git clone https://github.com/hungtq287/react_native_real_world.git -b master
```
- ios
  - Install dependencies
  ```sh
  $ cd ios
  $ pod install
  ```
  - launch ios app
  ```sh
  $ cd react_native_real_world
  $ react-native run-ios
  ```
# Tricks
- [Live Reload](https://stackoverflow.com/questions/47515695/how-start-watchman-in-react-native-project)